extern crate nalgebra as na;
extern crate serde_json;
extern crate wide;

use crate::soft_engine::get_face;
use crate::soft_engine::Texture;
use crate::soft_engine::Vertex;
use std::io::Error;
use na::{Point2, Point3, Vector3};
use serde_json::Value;

use std::fs::File;
use std::io::BufReader;
use std::path::Path;
use std::collections::HashMap;
use crate::soft_engine::{Face, Mesh};


fn read_json_from_file<P: AsRef<Path>>(path: P) -> Result<Value, Error> {
    // Open the file in read-only mode with buffer.
    let file = File::open(path)?;
    let reader = BufReader::new(file);

    // Read the JSON contents of the file as an instance of `User`.
    let value = serde_json::from_reader(reader)?;

    // Return the generic json loaded into a Value.
    Ok(value)
}

/// WARNING: this may panic, I really didn't care about this
/// and I only wrote it for testing purposes... so I used unwrap
/// without checking anything.
pub fn import_meshes<P: AsRef<Path>>(path: P) -> Vec<Mesh> {
    let json_object = match read_json_from_file(path) {
        Ok(json) => json,
        Err(err) => {
            println!("There was an error reading the json file {:?}", err);
            return Vec::<Mesh>::new();
            },
    };
    
    let meshes = json_object["meshes"].as_array().unwrap();
    let materials_array = json_object["materials"].as_array().unwrap();
    let mut materials = HashMap::new();
    
    for material in materials_array.iter() {
        // material["Name"] is unused.
        let id = String::from(material["id"].as_str().unwrap());
        let texture_object = material["diffuseTexture"].as_object().unwrap();
        materials.insert(id, texture_object["name"].as_str().unwrap());
    }
    let mut new_meshes : Vec<Mesh>= Vec::new();
    for mesh in meshes {
        let vertices_array = mesh["vertices"].as_array().unwrap();
        // Faces
        let indices_array = mesh["indices"].as_array().unwrap();
        

        let mut vertices_step = 1;

        // Depending of the number of texture's coordinates per vertex
        // we're jumping in the vertices array  by 6, 8 & 10 windows frame
        let uv_count = mesh["uvCount"].as_u64().unwrap();
        match uv_count {
            0=> {
                vertices_step = 6;
            },
            1=> {
                vertices_step = 8;
            },
            2=> {
                vertices_step = 10;
            },
            _=> ()
        }

        // the number of interesting vertices information for us
        let vertices_count = vertices_array.len() / vertices_step;
        // number of faces is logically the size of the array divided by 3 (A, B, C)
        let faces_count = indices_array.len() / 3;
        
        let mut new_mesh = Mesh{
            name: mesh["name"].as_str().unwrap().to_owned(),
            position: Point3::<f32>::new(0.0,0.0,0.0),
            rotation: Point3::<f32>::new(0.0,0.0,0.0),
            vertices: vec![Vertex {
                normal:Vector3::new(0.0,0.0,0.0),
                coordinates:Point3::<f32>::new(0.0,0.0,0.0),
                world_coordinates:Point3::<f32>::new(0.0,0.0,0.0),
                texture_coordinates:Point2::<f32>::new(0.0,0.0)
            }; vertices_count],
            faces:vec![Face{a:0,b:0,c:0, normal: Vector3::new(0.0,0.0,0.0)}; faces_count],
            texture: None,
            };
        
        // Filling the Vertices array of our mesh first
        for index in 0..vertices_count {
            let x = vertices_array[index * vertices_step].as_f64().unwrap();
            let y = vertices_array[index * vertices_step + 1].as_f64().unwrap();
            let z = vertices_array[index * vertices_step + 2].as_f64().unwrap();
                // Loading the vertex normal exported by Blender
            let nx = vertices_array[index * vertices_step + 3].as_f64().unwrap();
            let ny = vertices_array[index * vertices_step + 4].as_f64().unwrap();
            let nz = vertices_array[index * vertices_step + 5].as_f64().unwrap();
            
            let texture_coordinates : Point2<f32>; 
            if uv_count > 0 {
                let u = vertices_array[index * vertices_step + 6].as_f64().unwrap();
                let v = vertices_array[index * vertices_step + 7].as_f64().unwrap();
                texture_coordinates = Point2::new(u as f32, v as f32);
            } else {
                texture_coordinates = Point2::new(0.0, 0.0);
            }
            new_mesh.vertices[index] = Vertex {
                normal:Vector3::new(nx as f32, ny as f32, nz as f32),
                coordinates:Point3::new(x as f32, y as f32, z as f32),
                world_coordinates:Point3::<f32>::new(0.0,0.0,0.0),
                texture_coordinates
            };
        }
                
        // Then filling the Faces array
        for index in 0..faces_count {
            let a = indices_array[index * 3].as_u64().unwrap();
            let b = indices_array[index * 3 + 1].as_u64().unwrap();
            let c = indices_array[index * 3 + 2].as_u64().unwrap();
            /*
            Let recalculate all normals instead of using this. Just in case...
            let mut normal = new_mesh.vertices[a as usize].normal 
                + new_mesh.vertices[b as usize].normal 
                + new_mesh.vertices[c as usize].normal;
            normal /= 3.0;
            normal = normal.normalize();*/
            new_mesh.faces[index] = get_face(&mut new_mesh.vertices, a as usize, b as usize, c as usize);
        }
                
        // Getting the position you've set in Blender
        let position = mesh["position"].as_array().unwrap();
        new_mesh.position = Point3::<f32>::new(position[0].as_f64().unwrap() as f32, position[1].as_f64().unwrap() as f32, position[2].as_f64().unwrap() as f32);
        
        if uv_count > 0 {
            let mesh_texture_id = mesh["materialId"].as_str().unwrap().to_owned();
            let mesh_texture_name = String::from("assets/") + materials[&mesh_texture_id];
            new_mesh.texture = Some(Texture::build(mesh_texture_name));
        }
                
        new_meshes.push(new_mesh);
    }
    new_meshes
}
