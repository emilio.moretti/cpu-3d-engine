#!/usr/bin/env sh
rm -rf ./target/release/cpu-3d-engine
RUSTFLAGS="-C target-cpu=native" cargo build --release
./target/release/cpu-3d-engine
