mod soft_engine;
mod babylon;
mod utils;


use crate::soft_engine::Render;
use crate::utils::get_time_in_millis;
use soft_engine::{Camera, Device};
use minifb::{Key, Window, WindowOptions};
use nalgebra::{Point3, Vector3};
use std::cell::RefCell;
use std::rc::Rc;
//use std::sync::Mutex;

const WIDTH: usize = 1920;
const HEIGHT: usize = 1080;

type KeyVec = Rc<RefCell<Vec<u32>>>;

struct Input {
    keys: KeyVec,
}

impl Input {
    fn new(data: &KeyVec) -> Input {
        Input { keys: data.clone() }
    }
}

impl minifb::InputCallback for Input {
    fn add_char(&mut self, uni_char: u32) {
        self.keys.borrow_mut().push(uni_char);
    }
}

fn main() {
    // #### MINIFB SETUP #####
    //let mut buffer: [u32;WIDTH * HEIGHT] = [0; WIDTH * HEIGHT];

    let mut window = Window::new(
        "Test - F12 to exit",
        WIDTH,
        HEIGHT,
        WindowOptions::default(),
    )
    .unwrap_or_else(|e| {
        panic!("{}", e);
    });

    let keys_data = KeyVec::new(RefCell::new(Vec::new()));

    let input = Box::new(Input::new(&keys_data));

    // Limit to max ~60 fps update rate
    //window.limit_update_rate(Some(std::time::Duration::from_micros(16600)));
    window.set_input_callback(input);
    
    // #### APP START ####
    let mut meshes = babylon::import_meshes("./assets/monkey.babylon");
    let camera = Camera {
        position: Point3::new(0.0, 0.0, 10.0),
        target: Point3::new(0.0, 0.0, 0.0)
    };
    let mut device = Device {
        back_buffer : vec![0; WIDTH * HEIGHT],
        //depth_buffer : std::iter::repeat_with(|| Mutex::new(0.0)).take(WIDTH * HEIGHT).collect(),
        depth_buffer : vec![0.0; WIDTH * HEIGHT],
        height : HEIGHT as u32,
        width : WIDTH as u32
    };
    let mut last_render_time : u128 = 0;
    // #### MINIFB MAIN LOOP ####
    while window.is_open() && !window.is_key_down(Key::F12) {
        // #### ROTATE CUBE ####
        let monkey = &mut meshes[0];
        
        monkey.rotation += Vector3::new(0.0, 0.01, 0.0);
        
        // #### RENDER TO BACK_BUFFER ####
        device.clear();
        device.render(&camera,&meshes);
        
        // #### PRESENT ####
        // We unwrap here as we want this code to exit if it fails. Real applications may want to handle this in a different way
        window.update_with_buffer(&device.present(), WIDTH, HEIGHT).unwrap();
        let now = get_time_in_millis();
        let fps = 1000 / (now - last_render_time);
        last_render_time = now;
        let mut keys = keys_data.borrow_mut();
        
        if window.is_key_down(Key::F) {
            println!("FPS {:?}", fps);
        }
        /*for t in keys.iter() {
            println!("keys {}", t);
        }*/

        keys.clear();
    }
}
