use std::time::UNIX_EPOCH;
use std::time::SystemTime;

pub fn get_time_in_millis() -> u128 {
    SystemTime::now().duration_since(UNIX_EPOCH).expect("Time went backwards").as_millis()
}