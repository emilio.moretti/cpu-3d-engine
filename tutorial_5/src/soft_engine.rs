extern crate nalgebra as na;
extern crate wide;

//use std::sync::Mutex;
use nalgebra::UnitQuaternion;
use nalgebra::Matrix4;
use nalgebra::Translation3;
use nalgebra::Point3;
use na::{Isometry3, Vector3};

pub struct Camera {
    pub position: Point3<f32>,
    pub target: Point3<f32>,
}

/// A face is composed by indices to
/// three vertices
#[derive(Clone, Debug)]
pub struct Face {
    pub a : usize,
    pub b : usize,
    pub c : usize
}

#[derive(Clone, Debug)]
pub struct Vertex {
    pub normal : Vector3<f32>,
    pub coordinates : Point3<f32>,
    pub world_coordinates : Point3<f32>,
}

#[derive(Clone, Debug)]
pub struct Mesh {
    pub name: String,
    pub position: Point3<f32>,
    pub rotation: Vector3<f32>,
    pub vertices: Vec<Vertex>,
    pub faces: Vec<Face>,
}

#[derive(Clone, Debug)]
pub struct Device {
    pub back_buffer: Vec<u32>,
    //pub depth_buffer: Vec<Mutex<f32>>, //thread safe option
    pub depth_buffer: Vec<f32>,
    pub height: u32,
    pub width: u32,
}

#[derive(Clone, Debug)]
pub struct InterpolationNDotLData {
    pub a: f32,
    pub b: f32,
    pub c: f32,
    pub d: f32,
    pub current_y: u32,
}

#[derive(Clone, Debug)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl From<Color> for u32 {
    fn from(item: Color) -> Self {
        let (r, g, b) = (item.r as u32, item.g as u32, item.b as u32);
        (r << 16) | (g << 8) | b
    }
}

/**
 * The encoding for each pixel is `0RGB`:
 * The upper 8-bits are ignored, the next 8-bits are for the red channel, the next 8-bits
 * afterwards for the green channel, and the lower 8-bits for the blue channel.
 */
fn minifb_color_from_u8_rgb(r: u8, g: u8, b: u8) -> u32 {
    let (r, g, b) = (r as u32, g as u32, b as u32);
    (r << 16) | (g << 8) | b
}

pub trait Render {
    fn clear(&mut self);
    fn present(&self) -> &[u32];
    fn put_pixel(&mut self, x:u32,y:u32,z:f32,color:&Color);
    fn draw_point(&mut self, x:u32,y:u32, z:f32, color:&Color);
    fn draw_bresenham_line(&mut self,x0:u32,y0:u32, x1:u32, y1:u32, color:&Color);
    fn render(&mut self, camera: &Camera, mesh: &[Mesh]);
    fn project(&self, vertex : &Vertex, mat : &Matrix4<f32>, world : &Matrix4<f32>) -> Vertex;
    fn clamp(&self, value:f32, min:f32, max :f32) -> f32;
    fn interpolate(&self, min:f32, max:f32, gradient:f32) -> f32;
    fn process_scan_line(&mut self, data: &InterpolationNDotLData, va:&Vertex, vb:&Vertex, vc:&Vertex, vd:&Vertex, color:&Color);
    fn draw_triangle(&mut self,p1:Vertex, p2:Vertex, p3:Vertex, color:&Color);
    fn compute_n_dot_l(&self, world_coords :Point3<f32>, normal:Vector3<f32>, light_position:Point3<f32>) -> f32;
}

impl Render for Device {
    fn clear(&mut self) {
        let color : u32 = minifb_color_from_u8_rgb(0,0,0);
        for (buffer,depth) in self.back_buffer.iter_mut().zip(self.depth_buffer.iter_mut()){
            *buffer = color;
            *depth = f32::MAX;
        }
        
        /*
        //run this in another thread? the framerate was 10x slower when using an array of rwlock
        for depth_lock in self.depth_buffer.iter(){
            let mut depth =  depth_lock.lock().unwrap(); // there are no other writers during clear
            *depth = f32::MAX;
        }*/
    }

    fn present(&self) -> &[u32]{
        &self.back_buffer
    }

    fn put_pixel(&mut self,x:u32,y:u32,z:f32,color:&Color){
        let index = (x+y * self.width) as usize;
        if self.depth_buffer[index] > z {
            self.back_buffer[index] = color.clone().into();
            self.depth_buffer[index] = z;
        }
    }
    /*
    fn put_pixel_thread_safe(&mut self,x:u32,y:u32,z:f32,color:u32){
        let index = (x+y * self.width) as usize;
        let mut depth = self.depth_buffer[index].lock().expect("the buffer lock is poisoned!! WHAAAAT?!!!");
        if *depth > z {
            self.back_buffer[index] = color;
            *depth = z;
        }
    }*/

    fn draw_point(&mut self,x:u32,y:u32, z:f32, color:&Color){
        if x < self.width && y < self.height {
            self.put_pixel(x,y, z, color)
        }
    }
    
    fn draw_bresenham_line(&mut self,x0:u32,y0:u32, x1 : u32, y1: u32, color:&Color){
        let mut ix = x0 as i64;
        let mut iy = y0 as i64;
        let dx : i64 = (x1  as i64 - x0 as i64).abs();
        let dy : i64 = (y1  as i64 - y0 as i64).abs();
        let sx : i64 = if x0 < x1 {1} else {-1};
        let sy : i64 = if y0 < y1 {1} else {-1};
        let mut err = dx - dy;
        loop {
            self.draw_point(ix as u32, iy as u32, 0.0, color);
            if (ix == x1 as i64) && (iy == y1 as i64) {
                break;
            }
            let e2 = 2 * err;
            if e2 > -dy {
                err -= dy;
                ix += sx; //two's complement. we should be fine.
            }
            if e2 < dx {
                err += dx;
                iy += sy;
            }
        }
    }
    /// part 4 bis suggests to speed up this process by runing threads that write
    /// on the back_buffer. this is not allowed in rust.
    /// We hace three optoins:
    /// array of RwLocks and unsafe write to back_buffer (like the tutorial)
    /// protect the back_buffer with RwLocks, and copy to a new array in self.present()
    /// after solving that consider using rayon par_iter_mut on the faces
    fn render(&mut self,camera:&Camera, meshes: &[Mesh]){
        //view
        let view   = Isometry3::look_at_lh(&camera.position, &camera.target, &Vector3::y());
        // A perspective projection.
        // Note that you could use Perspective3 here, and then call to .as_matrix()
        let mut projection = Matrix4::new_perspective(self.width as f32 /self.height as f32, 0.78, 0.1, 1.0);
        
        //Convert to left-handed. Nalgebra is right-handed
        projection[(2,2)] *= -1.0;
        projection[(3,2)] *= -1.0;
        
        for mesh in meshes {
            // Our object is translated along the x axis.
            //by the way... this should be z, x, y. I don't know why I need to send it like this
            // to get the same visual rotation as the tutorial.
            let world_matrix =  UnitQuaternion::from_euler_angles(mesh.rotation.x, mesh.rotation.y, mesh.rotation.z) * Translation3::new(mesh.position.x,mesh.position.y,mesh.position.z);

            // The combination of the model with the view is still an isometry.
            let model_view = view * world_matrix;
            let mat_model_view = model_view.to_homogeneous();
            // Combine everything.
            let model_view_projection : Matrix4<f32> = projection * mat_model_view;
            let color=Color{r:180,g:180,b:180,a:255};
            for face in mesh.faces.iter() {
                //let variable_color : u8 = (255.0 * (0.25 + (face_index as f32 % mesh.faces.len() as f32) * 0.75 / mesh.faces.len() as f32)) as u8;
                let point_a = self.project(&mesh.vertices[face.a], &model_view_projection, &world_matrix.to_homogeneous());
                let point_b = self.project(&mesh.vertices[face.b], &model_view_projection, &world_matrix.to_homogeneous());
                let point_c = self.project(&mesh.vertices[face.c], &model_view_projection, &world_matrix.to_homogeneous());
                //self.draw_bresenham_line(point_a.x as u32, point_a.y as u32, point_b.x as u32, point_b.y as u32,color);
                //self.draw_bresenham_line(point_b.x as u32, point_b.y as u32, point_c.x as u32, point_c.y as u32,color);
                //self.draw_bresenham_line(point_c.x as u32, point_c.y as u32, point_a.x as u32, point_a.y as u32,color);
                self.draw_triangle(point_a, point_b, point_c, &color);
            }
        }

    }
    fn project(&self, vertex : &Vertex, mat : &Matrix4<f32>, world : &Matrix4<f32>) -> Vertex{
            let point = mat.transform_point(&vertex.coordinates);
            let world_coord = world.transform_point(&vertex.coordinates);
            let world_normal = world.transform_vector(&vertex.normal);
            // The transformed coordinates will be based on coordinate system
            // starting on the center of the screen. But drawing on screen normally starts
            // from top left. We then need to transform them again to have x:0, y:0 on top left.
            let x = point.x * self.width as f32 + self.width as f32 / 2.0;
            let y = -point.y * self.height as f32 + self.height as f32 / 2.0;
            
            Vertex {
                normal:world_normal,
                coordinates:Point3::new(x,y,point.z),
                world_coordinates:world_coord
            }
    }
    
    fn compute_n_dot_l(&self, world_coords :Point3<f32>, normal:Vector3<f32>, light_position:Point3<f32>) -> f32 {
        let light_direction = light_position - world_coords;

        let n_normal = normal.normalize();
        let n_light_dir = light_direction.normalize();

        (n_normal.dot(&n_light_dir)).max(0.0)
    }

    // Clamping values to keep them between 0 and 1
    fn clamp(&self, value:f32, min:f32, max :f32) -> f32 {
        min.max(value.min(max))
    }
    
    // Interpolating the value between 2 vertices 
    // min is the starting point, max the ending point
    // and gradient the % between the 2 points
    fn interpolate(&self, min:f32, max:f32, gradient:f32) -> f32 {
        min + (max - min) * self.clamp(gradient,0.0,1.0)
    }
    
    // drawing line between 2 points from left to right
    // papb -> pcpd
    // pa, pb, pc, pd must then be sorted before
    fn process_scan_line(&mut self, data: &InterpolationNDotLData, va:&Vertex, vb:&Vertex, vc:&Vertex, vd:&Vertex, color:&Color)
    {
        let pa = va.coordinates;
        let pb = vb.coordinates;
        let pc = vc.coordinates;
        let pd = vd.coordinates;
        // Thanks to current Y, we can compute the gradient to compute others values like
        // the starting X (sx) and ending X (ex) to draw between
        // if pa.y == pb.y or pc.y == pd.y, gradient is forced to 1
        let gradient1 = if pa.y as u32 != pb.y as u32 {(data.current_y as f32 - pa.y) / (pb.y - pa.y)} else {1.0};
        let gradient2 = if pc.y as u32 != pd.y as u32 {(data.current_y as f32 - pc.y) / (pd.y - pc.y)} else {1.0};
                
        let sx = self.interpolate(pa.x, pb.x, gradient1) as u32;
        let ex = self.interpolate(pc.x, pd.x, gradient2) as u32;
    
    
        // starting Z & ending Z
        let z1 = self.interpolate(pa.z, pb.z, gradient1);
        let z2 = self.interpolate(pc.z, pd.z, gradient2);
        
        let snl = self.interpolate(data.a, data.b, gradient1);
        let enl = self.interpolate(data.c, data.d, gradient2);
        
        // drawing a line from left (sx) to right (ex) 
        for x in sx..ex
        {
            let gradient = (x - sx) as f32 / (ex - sx) as f32;
            let z = self.interpolate(z1, z2, gradient);
            let ndotl = self.interpolate(snl, enl, gradient);
            self.draw_point(x, data.current_y, z, &Color{
                r:(color.r as f32 * ndotl) as u8,
                g:(color.g as f32 * ndotl) as u8, 
                b:(color.b as f32 * ndotl) as u8, 
                a:255
                });
        }
    }
    
    fn draw_triangle(&mut self,point1:Vertex, point2:Vertex, point3:Vertex, color:&Color){
        // Sorting the points in order to always have this order on screen p1, p2 & p3
        // with p1 always up (thus having the Y the lowest possible to be near the top screen)
        // then p2 between p1 & p3
        let (mut v1,mut v2,mut v3) = (point1,point2,point3);
        if v1.coordinates.y > v2.coordinates.y
        {
            std::mem::swap(&mut v2, &mut v1);
        }
    
        if v2.coordinates.y > v3.coordinates.y
        {
            std::mem::swap(&mut v3, &mut v2);
        }
    
        if v1.coordinates.y > v2.coordinates.y
        {
            std::mem::swap(&mut v2, &mut v1);
        }
    
        let p1 = v1.coordinates;
        let p2 = v2.coordinates;
        let p3 = v3.coordinates;
        let light_pos = Point3::new(0.0, 10.0, 10.0);
        
        let nl1 = self.compute_n_dot_l(v1.world_coordinates, v1.normal, light_pos);
        let nl2 = self.compute_n_dot_l(v2.world_coordinates, v2.normal, light_pos);
        let nl3 = self.compute_n_dot_l(v3.world_coordinates, v3.normal, light_pos);
        
        let mut data = InterpolationNDotLData{
            a:0.0,b:0.0,c:0.0,d:0.0,current_y:0
        };
        // inverse slopes
        let d_p1_p2 :f32;
        let d_p1_p3 : f32;
    
        // http://en.wikipedia.org/wiki/Slope
        // Computing inverse slopes
        if p2.y - p1.y > 0.0 {
            d_p1_p2 = (p2.x - p1.x) / (p2.y - p1.y);
        }
        else {
            d_p1_p2 = 0.0;
        }
        if p3.y - p1.y > 0.0 {
            d_p1_p3 = (p3.x - p1.x) / (p3.y - p1.y);
        }
        else {
            d_p1_p3 = 0.0;
        }
    
        // First case where triangles are like that:
        // P1
        // -
        // -- 
        // - -
        // -  -
        // -   - P2
        // -  -
        // - -
        // -
        // P3
        if d_p1_p2 > d_p1_p3
        {
            for y in p1.y as u32..=p3.y as u32 {
                data.current_y = y;
                
                if p2.y > y as f32 {
                    data.a = nl1;
                    data.b = nl3;
                    data.c = nl1;
                    data.d = nl2;
                    self.process_scan_line(&data, &v1,&v3,&v1,&v2, color);
                }
                else {
                    data.a = nl1;
                    data.b = nl3;
                    data.c = nl2;
                    data.d = nl3;
                    self.process_scan_line(&data, &v1,&v3,&v2,&v3, color);
                }
            }
        }
        // First case where triangles are like that:
        //       P1
        //        -
        //       -- 
        //      - -
        //     -  -
        // P2 -   - 
        //     -  -
        //      - -
        //        -
        //       P3
        else {
            for y in p1.y as u32..=p3.y as u32 {
                data.current_y = y;
                
                if p2.y > y as f32 {
                    data.a = nl1;
                    data.b = nl2;
                    data.c = nl1;
                    data.d = nl3;
                    self.process_scan_line(&data, &v1,&v2,&v1,&v3, color);
                }
                else {
                    data.a = nl2;
                    data.b = nl3;
                    data.c = nl1;
                    data.d = nl3;
                    self.process_scan_line(&data, &v2,&v3,&v1,&v3, color);
                }
            }
        }
    }
}

/*
pub fn create_cube() -> Mesh {
    Mesh {
        name: "Cube".to_string(),
        vertices : vec!(
            Point3::new(-1.0, 1.0, 1.0),
            Point3::new(1.0, 1.0, 1.0),
            Point3::new(-1.0, -1.0, 1.0),
            Point3::new(1.0, -1.0, 1.0),
            Point3::new(-1.0, 1.0, -1.0),
            Point3::new(1.0, 1.0, -1.0),
            Point3::new(1.0, -1.0, -1.0),
            Point3::new(-1.0, -1.0, -1.0),
        ),
        rotation:Vector3::new(0.0, 0.0, 0.0),
        position:Point3::new(0.0, 0.0, 0.0),
        faces: vec!(
            Face { a : 0, b : 1, c : 2 },
            Face { a : 1, b : 2, c : 3 },
            Face { a : 1, b : 3, c : 6 },
            Face { a : 1, b : 5, c : 6 },
            Face { a : 0, b : 1, c : 4 },
            Face { a : 1, b : 4, c : 5 },
            Face { a : 2, b : 3, c : 7 },
            Face { a : 3, b : 6, c : 7 },
            Face { a : 0, b : 2, c : 7 },
            Face { a : 0, b : 4, c : 7 },
            Face { a : 4, b : 5, c : 6 },
            Face { a : 4, b : 6, c : 7 }
        ),
    }
}
*/