# cpu-3d-engine

Ported this tutorial to Rust (stable) using nalgebra and minifb.
<<Tutorial series: learning how to write a 3D soft engine from scratch in C#, TypeScript or JavaScript>>

https://www.davrous.com/2013/06/13/tutorial-series-learning-how-to-write-a-3d-soft-engine-from-scratch-in-c-typescript-or-javascript/

Compile and run with `build_and_run.sh` script.
Press F to show the framerate in the console.

The final code runs at 90FPS @ 1920x1080 in a Ryzen 7 1800x CPU, with some FPS drops depending on the visible side of the model.


Rules:
* Rust stable only
* No unsafe code
* the code under tutorial_* folders must match the tutorial as close as possible.
* 3D Engine code can be refactored as much as required to make it faster (as long as it follows the other rules)

Feel free to send optimization tricks and new changes. I only replaced a few array loops with iterators to take advantage of the auto-vectorization.

About the tutorials in general:
The original code had some issues, like mixed vertex order declarations on the models (clockwise and counter clockwise in the cube, and also in the 3D babylon model).
There is also a mix up on the data types (this one from me while I was learning nalgebra). I should have used Vector3 for the normals, and Point3 for the rest of the attributes.The perspective matrix was partially converted to left handled on the tutorials instead of inverting the entire column sign.
You will find details like this on all tutorials folders, feel free to send fixes, but it's recommended to use the code from cpu_3d_engine, which has all these details properly fixed.

About tutorial_4_bis:
While the tutorial suggests using locks to access the buffer from several threads to get a small FPS improvement, a quick
test with Mutex showed the framerate dropped drastically to around 15FPS, so I didn't follow that path. The code is unfinished,
and that's why you will see there is no Arc trait nor any threads.
