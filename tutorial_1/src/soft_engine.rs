extern crate nalgebra as na;
extern crate wide;

use nalgebra::UnitQuaternion;
use nalgebra::Matrix4;
use nalgebra::Translation3;
use nalgebra::Point2;
use nalgebra::Point3;
use na::{Isometry3, Vector3};


pub struct Camera {
    pub position: Point3<f32>,
    pub target: Point3<f32>,
}

pub struct Mesh {
    pub name: String,
    pub position: Point3<f32>,
    pub rotation: Vector3<f32>,
    pub vertices: Vec<Point3<f32>>,
}

pub struct Device {
    pub back_buffer: Vec<u32>,
    pub height: u32,
    pub width: u32,
}

/**
 * The encoding for each pixel is `0RGB`:
 * The upper 8-bits are ignored, the next 8-bits are for the red channel, the next 8-bits
 * afterwards for the green channel, and the lower 8-bits for the blue channel.
 */
fn minifb_color_from_u8_rgb(r: u8, g: u8, b: u8) -> u32 {
    let (r, g, b) = (r as u32, g as u32, b as u32);
    (r << 16) | (g << 8) | b
}

pub trait Render {
    fn clear(&mut self);
    fn present(&self) -> &[u32];
    fn put_pixel(&mut self, x:u32,y:u32,color:u32);
    fn draw_point(&mut self, x:u32,y:u32);
    fn render(&mut self, camera: &Camera, mesh: &[Mesh]);
    fn project(&self, coord:&Point3<f32>, mat : &Matrix4<f32>) -> Point2<u32>;
}

impl Render for Device {
    fn clear(&mut self) {
        let color : u32 = minifb_color_from_u8_rgb(0,0,0);
        for i in 0..self.back_buffer.len(){
            self.back_buffer[i] = color;
        }
    }
    fn present(&self) -> &[u32]{
        &self.back_buffer //we don't copy anything... I don't know if this will work
    }
    
    fn put_pixel(&mut self,x:u32,y:u32,color:u32){
        self.back_buffer[(x+y * self.width) as usize] = color;
    }
    fn draw_point(&mut self,x:u32,y:u32){
        if x < self.width && y < self.height
            {
                self.put_pixel(x,y, minifb_color_from_u8_rgb(255,255,255))
            }
    }
    fn render(&mut self,camera:&Camera, meshes: &[Mesh]){
        //view
        let view   = Isometry3::look_at_lh(&camera.position, &camera.target, &Vector3::y());

        // A perspective projection.
        // Note that you could use Perspective3 here, and then call to .as_matrix()
        let mut projection = Matrix4::new_perspective(self.width as f32 /self.height as f32, 0.78, 0.1, 1.0);
        
        //Convert to left-handed. Nalgebra is right-handed
        projection[(2,2)] *= -1.0;
        projection[(3,2)] *= -1.0;
        
        for mesh in meshes {
            // Our object is translated along the x axis.
            let world_matrix =  UnitQuaternion::from_euler_angles(mesh.rotation.x, mesh.rotation.y, mesh.rotation.z) * Translation3::new(mesh.position.x,mesh.position.y,mesh.position.z);

            // The combination of the model with the view is still an isometry.
            let model_view = view * world_matrix;
            let mat_model_view = model_view.to_homogeneous();
            // Combine everything.
            let model_view_projection : Matrix4<f32> = projection * mat_model_view;
            for vertex in &mesh.vertices {
                let point = self.project(&vertex, &model_view_projection);
                self.draw_point(point.x, point.y);
            }
        }
    }
    fn project(&self, coord:&Point3<f32>, mat : &Matrix4<f32>) -> Point2<u32>{
            let point = mat.transform_point(&coord);
            // The transformed coordinates will be based on coordinate system
            // starting on the center of the screen. But drawing on screen normally starts
            // from top left. We then need to transform them again to have x:0, y:0 on top left.
            let x = point.x * self.width as f32 + self.width as f32 / 2.0;
            let y = -point.y * self.height as f32 + self.height as f32 / 2.0;
            Point2::new(x as u32, y as u32)
    }
}

pub fn create_cube() -> Mesh {
    Mesh {
        name: "Cube".to_string(),
        vertices : vec!(
            Point3::new(-1.0, 1.0, 1.0),
            Point3::new(1.0, 1.0, 1.0),
            Point3::new(-1.0, -1.0, 1.0),
            Point3::new(1.0, -1.0, 1.0),
            Point3::new(-1.0, 1.0, -1.0),
            Point3::new(1.0, 1.0, -1.0),
            Point3::new(1.0, -1.0, -1.0),
            Point3::new(-1.0, -1.0, -1.0),
        ),
        rotation:Vector3::new(0.0, 0.0, 0.0),
        position:Point3::new(0.0, 0.0, 0.0),
    }
}