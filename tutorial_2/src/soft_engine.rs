extern crate nalgebra as na;
extern crate wide;

use nalgebra::UnitQuaternion;
use nalgebra::Matrix4;
use nalgebra::Translation3;
use nalgebra::Point2;
use nalgebra::Point3;
use na::{Isometry3, Vector3};


pub struct Camera {
    pub position: Point3<f32>,
    pub target: Point3<f32>,
}

/// A face is composed by indices to
/// three vertices
pub struct Face {
    a : usize,
    b : usize,
    c : usize
}
pub struct Mesh {
    pub name: String,
    pub position: Point3<f32>,
    pub rotation: Vector3<f32>,
    pub vertices: Vec<Point3<f32>>,
    pub faces: Vec<Face>,
}

pub struct Device {
    pub back_buffer: Vec<u32>,
    pub height: u32,
    pub width: u32,
}

/**
 * The encoding for each pixel is `0RGB`:
 * The upper 8-bits are ignored, the next 8-bits are for the red channel, the next 8-bits
 * afterwards for the green channel, and the lower 8-bits for the blue channel.
 */
fn minifb_color_from_u8_rgb(r: u8, g: u8, b: u8) -> u32 {
    let (r, g, b) = (r as u32, g as u32, b as u32);
    (r << 16) | (g << 8) | b
}

pub trait Render {
    fn clear(&mut self);
    fn present(&self) -> &[u32];
    fn put_pixel(&mut self, x:u32,y:u32,color:u32);
    fn draw_point(&mut self, x:u32,y:u32);
    fn draw_bresenham_line(&mut self,x0:u32,y0:u32, x1:u32, y1:u32);
    fn render(&mut self, camera: &Camera, mesh: &[Mesh]);
    fn project(&self, coord:&Point3<f32>, mat : &Matrix4<f32>) -> Point2<u32>;
}

impl Render for Device {
    fn clear(&mut self) {
        let color : u32 = minifb_color_from_u8_rgb(0,0,0);
        for i in 0..self.back_buffer.len(){
            self.back_buffer[i] = color;
        }
    }
    fn present(&self) -> &[u32]{
        &self.back_buffer //we don't copy anything... I don't know if this will work
    }
    
    fn put_pixel(&mut self,x:u32,y:u32,color:u32){
        self.back_buffer[(x+y * self.width) as usize] = color;
    }
    fn draw_point(&mut self,x:u32,y:u32){
        if x < self.width && y < self.height
            {
                self.put_pixel(x,y, minifb_color_from_u8_rgb(255,255,255))
            }
    }
    
    fn draw_bresenham_line(&mut self,x0:u32,y0:u32, x1 : u32, y1: u32){
    
    let mut ix:i64 = x0 as i64;
    let mut iy:i64 = y0 as i64;
    let dx : i64 = (x1  as i64 - x0 as i64).abs();
    let dy : i64 = (y1  as i64 - y0 as i64).abs();
    let sx : i64 = if x0 < x1 {1} else {-1};
    let sy : i64 = if y0 < y1 {1} else {-1};
    let mut err = dx - dy;
    loop {
        self.draw_point(ix as u32, iy as u32);

        if (ix == x1 as i64) && (iy == y1 as i64) {
            break;
        }
        let e2 = 2 * err;
        if e2 > -dy {
            err -= dy;
            ix += sx; //two's complement. we should be fine.
        }
        if e2 < dx {
            err += dx;
            iy += sy;
            }
    }
    }
    fn render(&mut self,camera:&Camera, meshes: &[Mesh]){
        //view
        let view   = Isometry3::look_at_lh(&camera.position, &camera.target, &Vector3::y());

        // A perspective projection.
        // Note that you could use Perspective3 here, and then call to .as_matrix()
        let mut projection = Matrix4::new_perspective(self.width as f32 /self.height as f32, 0.78, 0.1, 1.0);
        
        //Convert to left-handed. Nalgebra is right-handed
        projection[(2,2)] *= -1.0;
        projection[(3,2)] *= -1.0;
        
        for mesh in meshes {
            // Our object is translated along the x axis.
            //by the way... this should be z, x, y. I don't know why I need to send it like this
            // to get the same values.
            let world_matrix =  UnitQuaternion::from_euler_angles(mesh.rotation.x, mesh.rotation.y, mesh.rotation.z) * Translation3::new(mesh.position.x,mesh.position.y,mesh.position.z);

            // The combination of the model with the view is still an isometry.
            let model_view = view * world_matrix;
            let mat_model_view = model_view.to_homogeneous();
            // Combine everything.
            let model_view_projection : Matrix4<f32> = projection * mat_model_view;
            for face in &mesh.faces {
                
                let point_a = self.project(&mesh.vertices[face.a], &model_view_projection);
                let point_b = self.project(&mesh.vertices[face.b], &model_view_projection);
                let point_c = self.project(&mesh.vertices[face.c], &model_view_projection);
                self.draw_bresenham_line(point_a.x, point_a.y, point_b.x, point_b.y);
                self.draw_bresenham_line(point_b.x, point_b.y, point_c.x, point_c.y);
                self.draw_bresenham_line(point_c.x, point_c.y, point_a.x, point_a.y);
            }
        }
    }
    fn project(&self, coord:&Point3<f32>, mat : &Matrix4<f32>) -> Point2<u32>{
            let point = mat.transform_point(&coord);
            // The transformed coordinates will be based on coordinate system
            // starting on the center of the screen. But drawing on screen normally starts
            // from top left. We then need to transform them again to have x:0, y:0 on top left.
            let x = point.x * self.width as f32 + self.width as f32 / 2.0;
            let y = -point.y * self.height as f32 + self.height as f32 / 2.0;
            Point2::new(x as u32, y as u32)
    }
}

pub fn create_cube() -> Mesh {
    Mesh {
        name: "Cube".to_string(),
        vertices : vec!(
            Point3::new(-1.0, 1.0, 1.0),
            Point3::new(1.0, 1.0, 1.0),
            Point3::new(-1.0, -1.0, 1.0),
            Point3::new(1.0, -1.0, 1.0),
            Point3::new(-1.0, 1.0, -1.0),
            Point3::new(1.0, 1.0, -1.0),
            Point3::new(1.0, -1.0, -1.0),
            Point3::new(-1.0, -1.0, -1.0),
        ),
        rotation:Vector3::new(0.0, 0.0, 0.0),
        position:Point3::new(0.0, 0.0, 0.0),
        faces: vec!(
            Face { a : 0, b : 1, c : 2 },
            Face { a : 1, b : 2, c : 3 },
            Face { a : 1, b : 3, c : 6 },
            Face { a : 1, b : 5, c : 6 },
            Face { a : 0, b : 1, c : 4 },
            Face { a : 1, b : 4, c : 5 },
            Face { a : 2, b : 3, c : 7 },
            Face { a : 3, b : 6, c : 7 },
            Face { a : 0, b : 2, c : 7 },
            Face { a : 0, b : 4, c : 7 },
            Face { a : 4, b : 5, c : 6 },
            Face { a : 4, b : 6, c : 7 }
        ),
    }
}